def setup(options):
    return 0
def execute(block, config):
    z_lin,k_lin,p_k_lin = (block.get_grid("matter_power_lin", "z","k_h", "P_k"))
    block.put_grid("matter_power_nl","z",z_lin,"k_h",k_lin,"P_k",p_k_lin)
    return 0

def cleanup(config):
    return 0
