from builtins import range
from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import RectBivariateSpline

power3ds =["galaxy_intrinsic_power","galaxy_power","intrinsic_power","matter_galaxy_power","matter_intrinsic_power","matter_power_lin","matter_power_nl"]

def setup(options):
    weylratio = options.get_string(option_section,"weyl_ratio_name","rescaled_weyl_ratio_lin")
    corrind = {}
    for power in power3ds:
        corrind.update({power:options.get_int(option_section,power,0)})
    return weylratio,corrind
    
def execute(block,config):
    weylratio,corrind = config
    zw, kw, R = block.get_grid(weylratio, "z", "k_h", "R")
    Rspl = RectBivariateSpline(zw,kw,R)
    for power in corrind.keys():
        if corrind[power] == 0:
            continue
        zp, kp, P = block.get_grid(power, "z", "k_h", "P_k")
        Rvl = Rspl(zp,kp)**(corrind[power]/2.0)
        P = P*Rvl
        #for i,z in enumerate(zp):
        #    for j,k in enumerate(kp):
        #        Rvl[i,j] = Rspl(z,k)
        #        P[i,j] = P[i,j]*Rvl[i,j]
        block.replace_grid(power, "z", zp, "k_h", kp, "P_k", P)
        print(power+" corrected by factor R_weyl^"+str(corrind[power]/2.0))
    return 0

